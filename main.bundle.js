webpackJsonp([1,4],{

/***/ 103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__auth_store__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthCallbackComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AuthCallbackComponent = /** @class */ (function () {
    function AuthCallbackComponent(activateRoute, router, http, store) {
        this.activateRoute = activateRoute;
        this.router = router;
        this.http = http;
        this.store = store;
    }
    AuthCallbackComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activateRoute.params.subscribe(function (params) {
            _this.qParams = _this.activateRoute.snapshot.queryParams;
            if (!_this.qParams["error"]) {
                if (_this.qParams["code"] != null) {
                    var body = {
                        code: _this.qParams["code"],
                        status: ''
                    };
                    var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["Headers"]({ 'Content-Type': 'application/json' });
                    var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["RequestOptions"]({ headers: headers });
                    _this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].apiUrl + '/servicem8/connect', JSON.stringify(body), options)
                        .map(function (res) { return res.json(); })
                        .subscribe(function (data) {
                        console.log("Saving data " + data.toString());
                        _this.store.dispatchVendorUUID(data.toString());
                        console.log("Navigating");
                        _this.router.navigate(['twilio-registration']);
                    });
                }
            }
            else {
            }
        });
    };
    AuthCallbackComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'auth-callback',
            template: __webpack_require__(240)
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__angular_http__["Http"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_http__["Http"]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__auth_store__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__auth_store__["a" /* AuthService */]) === "function" && _d || Object])
    ], AuthCallbackComponent);
    return AuthCallbackComponent;
    var _a, _b, _c, _d;
}());

// let headers = new Headers({ 'Content-Type': 'application/json' });
// let options = new RequestOptions({ headers: headers });
// var promise = this.http.post(environment.apiUrl + '/servicem8/connect',
//     JSON.stringify(body), options)
//     .subscribe(data => {
//         // console.log("Saving data " + data)
//         // this.store.saveJobCategories(data);
//         // this.router.navigate(['category-selection']);
//     }); 
//# sourceMappingURL=auth-callback.component.js.map

/***/ }),

/***/ 104:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__environments_environment__ = __webpack_require__(41);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthInitiateComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthInitiateComponent = /** @class */ (function () {
    function AuthInitiateComponent() {
    }
    AuthInitiateComponent.prototype.ngOnInit = function () { };
    AuthInitiateComponent.prototype.onClick = function () {
        //let scopes = 'read_jobs manage_tasks vendor vendor_logo vendor_email publish_email read_customer_contacts read_job_contacts read_customers read_staff publish_sms';
        var scopes = 'vendor manage_jobs vendor_email';
        var params = new URLSearchParams();
        params.set('scope', scopes.toString()); // the user's search value
        params.set('redirect_uri', __WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].hostUrl + '/callback');
        params.set('response_type', 'code');
        var clientid = '790742';
        if (__WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].production) {
            clientid = "618030";
        }
        params.set('client_id', clientid);
        location.href = 'https://www.servicem8.com/oauth/authorize?' + params.toString();
    };
    AuthInitiateComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'auth-initiate',
            template: __webpack_require__(241)
        }),
        __metadata("design:paramtypes", [])
    ], AuthInitiateComponent);
    return AuthInitiateComponent;
}());

//# sourceMappingURL=auth-initiate.component.js.map

/***/ }),

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthSuccessComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AuthSuccessComponent = /** @class */ (function () {
    function AuthSuccessComponent() {
    }
    AuthSuccessComponent.prototype.ngOnInit = function () { };
    AuthSuccessComponent.prototype.onClick = function () {
        location.href = 'https://go.servicem8.com';
    };
    AuthSuccessComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'auth-success',
            template: __webpack_require__(242)
        }),
        __metadata("design:paramtypes", [])
    ], AuthSuccessComponent);
    return AuthSuccessComponent;
}());

//# sourceMappingURL=auth-success.component.js.map

/***/ }),

/***/ 106:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__auth_store__ = __webpack_require__(40);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TwilioMessagingOptionsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TwilioMessagingOptionsComponent = /** @class */ (function () {
    function TwilioMessagingOptionsComponent(authService) {
        this.authService = authService;
        this.jobCreationReplyError = false;
        this.numberSelectionError = false;
        this.categorySelectionError = false;
        this.identifier = "";
        this.vendorName = "";
        this.default_message = "Auto-reply from " + this.vendorName + ": We have processed your message. Job ID is: [JOB_ID].";
        this.jobCreationReply = false;
        this.jobCreation = true; // for now as there is only one thing to do atm
        this.statusReply = false;
        this.scheduleReply = false;
    }
    TwilioMessagingOptionsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authService.vendorUUID.subscribe(function (s) { return _this.identifier = s; });
        this.authService.categoriesServiceM8.subscribe(function (s) { return _this.servicem8Categories = s; });
        this.authService.numbersTwilio.subscribe(function (s) { return _this.twilioNumbers = s; });
        this.authService.vendorName.subscribe(function (s) { return _this.vendorName = s; });
        this.default_message = "Auto-reply from " + this.vendorName + ": We have processed your message. Job ID is: [JOB_ID].";
    };
    TwilioMessagingOptionsComponent.prototype.jobCreationReplySelect = function () {
        this.jobCreationReply = !this.jobCreationReply;
    };
    TwilioMessagingOptionsComponent.prototype.saveOptions = function () {
        if (this.noErrors()) {
            this.authService.saveTwilioOptions(this.identifier, this.selectedTwilioNumber, this.jobCreation, this.selectedJobCategory, this.jobCreationReply, this.creationReplyText, this.statusReply, this.scheduleReply);
        }
    };
    TwilioMessagingOptionsComponent.prototype.jobStatusReplySelect = function () {
        this.statusReply = !this.statusReply;
        this.setDefaultMessage();
    };
    TwilioMessagingOptionsComponent.prototype.jobScheduleReplySelect = function () {
        this.scheduleReply = !this.scheduleReply;
        this.setDefaultMessage();
    };
    TwilioMessagingOptionsComponent.prototype.setDefaultMessage = function () {
        this.default_message = "Auto-reply from " + this.vendorName + ": We have processed your message. Job ID is: [JOB_ID].";
        if (this.statusReply == true && this.scheduleReply == true) {
            this.default_message = this.default_message + " Text !STATUS or !WHEN followed by your job id for a status update or appointment date";
        }
        else if (this.statusReply == true && this.scheduleReply == false) {
            this.default_message = this.default_message + " Text !STATUS followed by your job id for a status update";
        }
        else if (this.statusReply == false && this.scheduleReply == true) {
            this.default_message = this.default_message + " Text !WHEN followed by your job id for your appointment date";
        }
    };
    TwilioMessagingOptionsComponent.prototype.noErrors = function () {
        if (this.selectedTwilioNumber == "" || this.selectedTwilioNumber == null) {
            this.numberSelectionError = true;
        }
        else {
            this.numberSelectionError = false;
        }
        if (this.selectedJobCategory == "" || this.selectedJobCategory == null) {
            this.categorySelectionError = true;
        }
        else {
            this.categorySelectionError = false;
        }
        if (this.jobCreationReply == true) {
            if (this.creationReplyText == "" || this.creationReplyText == null) {
                this.jobCreationReplyError = true;
            }
            else {
                this.jobCreationReplyError = false;
            }
        }
        else {
            this.jobCreationReplyError = false;
        }
        if (this.categorySelectionError == false && this.numberSelectionError == false && this.jobCreationReplyError == false) {
            return true;
        }
        else {
            return false;
        }
    };
    TwilioMessagingOptionsComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'twilio-messaging-options',
            template: __webpack_require__(243)
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__auth_store__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__auth_store__["a" /* AuthService */]) === "function" && _a || Object])
    ], TwilioMessagingOptionsComponent);
    return TwilioMessagingOptionsComponent;
    var _a;
}());

//# sourceMappingURL=twilio-messaging-options.component.js.map

/***/ }),

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__auth_store__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(18);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TwilioRegistrationComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TwilioRegistrationComponent = /** @class */ (function () {
    function TwilioRegistrationComponent(authService, fb) {
        this.authService = authService;
        this.fb = fb;
        this.twoInputs = { SidToken: '', AuthToken: '' };
        this.identifier = "";
    }
    TwilioRegistrationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authService.vendorUUID.subscribe(function (s) { return _this.identifier = s; });
        this.twilioForm = new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["j" /* FormGroup */]({
            'SidToken': new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["k" /* FormControl */](this.twoInputs.SidToken, [
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["i" /* Validators */].required,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["i" /* Validators */].minLength(34),
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["i" /* Validators */].pattern("A{1}C{1}[A-Za-z0-9]{32}")
            ]),
            'AuthToken': new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["k" /* FormControl */](this.twoInputs.AuthToken, [
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["i" /* Validators */].required,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["i" /* Validators */].minLength(32)
            ])
        });
    };
    Object.defineProperty(TwilioRegistrationComponent.prototype, "SidToken", {
        get: function () { return this.twilioForm.get('SidToken'); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(TwilioRegistrationComponent.prototype, "AuthToken", {
        get: function () { return this.twilioForm.get('AuthToken'); },
        enumerable: true,
        configurable: true
    });
    TwilioRegistrationComponent.prototype.register = function () {
        console.log(this.twilioForm.get('SidToken').value);
        console.log(this.twilioForm.get('AuthToken').value);
        //don't need the bool error checks, since the form self validates
        this.authService.registerTwilio(this.identifier, this.twilioForm.get('SidToken').value, this.twilioForm.get('AuthToken').value);
    };
    TwilioRegistrationComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'twilio-registration',
            template: __webpack_require__(244),
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__auth_store__["a" /* AuthService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__auth_store__["a" /* AuthService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_forms__["l" /* FormBuilder */]) === "function" && _b || Object])
    ], TwilioRegistrationComponent);
    return TwilioRegistrationComponent;
    var _a, _b;
}());

var CategoryObject = /** @class */ (function () {
    function CategoryObject() {
    }
    return CategoryObject;
}());
//# sourceMappingURL=twilio-registration.component.js.map

/***/ }),

/***/ 138:
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 138;


/***/ }),

/***/ 139:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_hammerjs__ = __webpack_require__(238);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_hammerjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_hammerjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__(180);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environments_environment__ = __webpack_require__(41);





if (__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 179:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app works!';
    }
    AppComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: '<router-outlet></router-outlet>'
        })
    ], AppComponent);
    return AppComponent;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 180:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_auth_initiate_component__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__auth_auth_callback_component__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__auth_twilio_registration_component__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__auth_auth_success_component__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__auth_twilio_messaging_options_component__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_component__ = __webpack_require__(179);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ngrx_store__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_reducers__ = __webpack_require__(181);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_flex_layout__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_material__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__angular_platform_browser_animations__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__app_routing__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_angular2_jwt__ = __webpack_require__(183);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_angular2_jwt___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_15_angular2_jwt__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__angular_http__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__auth_auth_store__ = __webpack_require__(40);
/* unused harmony export authHttpServiceFactory */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


















function authHttpServiceFactory(http, options) {
    return new __WEBPACK_IMPORTED_MODULE_15_angular2_jwt__["AuthHttp"](new __WEBPACK_IMPORTED_MODULE_15_angular2_jwt__["AuthConfig"]({}), http, options);
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_14__app_routing__["a" /* routing */],
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_16__angular_http__["HttpModule"],
                __WEBPACK_IMPORTED_MODULE_13__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_9__ngrx_store__["a" /* StoreModule */].provideStore(__WEBPACK_IMPORTED_MODULE_10__app_reducers__["a" /* reducer */]),
                __WEBPACK_IMPORTED_MODULE_11__angular_flex_layout__["a" /* FlexLayoutModule */],
                __WEBPACK_IMPORTED_MODULE_12__angular_material__["a" /* MatButtonModule */], __WEBPACK_IMPORTED_MODULE_12__angular_material__["b" /* MatCheckboxModule */], __WEBPACK_IMPORTED_MODULE_12__angular_material__["c" /* MatCardModule */], __WEBPACK_IMPORTED_MODULE_12__angular_material__["d" /* MatInputModule */], __WEBPACK_IMPORTED_MODULE_12__angular_material__["e" /* MatProgressSpinnerModule */], __WEBPACK_IMPORTED_MODULE_12__angular_material__["f" /* MatRadioModule */], __WEBPACK_IMPORTED_MODULE_12__angular_material__["g" /* MatSelectModule */]
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_3__auth_auth_initiate_component__["a" /* AuthInitiateComponent */],
                __WEBPACK_IMPORTED_MODULE_4__auth_auth_callback_component__["a" /* AuthCallbackComponent */],
                __WEBPACK_IMPORTED_MODULE_5__auth_twilio_registration_component__["a" /* TwilioRegistrationComponent */],
                __WEBPACK_IMPORTED_MODULE_6__auth_auth_success_component__["a" /* AuthSuccessComponent */], __WEBPACK_IMPORTED_MODULE_7__auth_twilio_messaging_options_component__["a" /* TwilioMessagingOptionsComponent */]
            ],
            providers: [
                {
                    provide: __WEBPACK_IMPORTED_MODULE_15_angular2_jwt__["AuthHttp"],
                    useFactory: authHttpServiceFactory,
                    deps: [__WEBPACK_IMPORTED_MODULE_16__angular_http__["Http"], __WEBPACK_IMPORTED_MODULE_16__angular_http__["RequestOptions"]]
                }, __WEBPACK_IMPORTED_MODULE_17__auth_auth_store__["a" /* AuthService */],
                { provide: __WEBPACK_IMPORTED_MODULE_12__angular_material__["h" /* MAT_PLACEHOLDER_GLOBAL_OPTIONS */], useValue: { float: 'always' } }
            ],
            exports: [],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 181:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ngrx_store__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__environments_environment__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth_auth_store__ = __webpack_require__(40);
/* harmony export (immutable) */ __webpack_exports__["a"] = reducer;



var reducers = {
    vendorUUID: __WEBPACK_IMPORTED_MODULE_2__auth_auth_store__["b" /* vendorUUID */], categoriesServiceM8: __WEBPACK_IMPORTED_MODULE_2__auth_auth_store__["c" /* categoriesServiceM8 */], numbersTwilio: __WEBPACK_IMPORTED_MODULE_2__auth_auth_store__["d" /* numbersTwilio */], vendorName: __WEBPACK_IMPORTED_MODULE_2__auth_auth_store__["e" /* vendorName */]
};
var developmentReducer = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["c" /* combineReducers */])(reducers);
var productionReducer = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__ngrx_store__["c" /* combineReducers */])(reducers);
function reducer(state, action) {
    if (__WEBPACK_IMPORTED_MODULE_1__environments_environment__["a" /* environment */].production) {
        return productionReducer(state, action);
    }
    else {
        return developmentReducer(state, action);
    }
}
//# sourceMappingURL=app.reducers.js.map

/***/ }),

/***/ 182:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__auth_auth_initiate_component__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth_auth_callback_component__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_twilio_registration_component__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__auth_twilio_messaging_options_component__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__auth_auth_success_component__ = __webpack_require__(105);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return routing; });






var appRoutes = [
    { path: 'initiate', component: __WEBPACK_IMPORTED_MODULE_1__auth_auth_initiate_component__["a" /* AuthInitiateComponent */] },
    { path: 'callback', component: __WEBPACK_IMPORTED_MODULE_2__auth_auth_callback_component__["a" /* AuthCallbackComponent */] },
    { path: 'twilio-registration', component: __WEBPACK_IMPORTED_MODULE_3__auth_twilio_registration_component__["a" /* TwilioRegistrationComponent */] },
    { path: 'twilio-messaging-options', component: __WEBPACK_IMPORTED_MODULE_4__auth_twilio_messaging_options_component__["a" /* TwilioMessagingOptionsComponent */] },
    { path: 'auth-success', component: __WEBPACK_IMPORTED_MODULE_5__auth_auth_success_component__["a" /* AuthSuccessComponent */] },
    {
        path: '',
        redirectTo: 'initiate',
        pathMatch: 'full',
    },
];
var routing = __WEBPACK_IMPORTED_MODULE_0__angular_router__["c" /* RouterModule */].forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map

/***/ }),

/***/ 240:
/***/ (function(module, exports) {

module.exports = "<div style=\"width:(100%); height: (100%)\">\r\n    <div fxLayoutAlign=\"center center\">\r\n        <mat-card style=\"width:400px; min-height: 400px\">\r\n            <div fxLayout=\"column\" fxLayoutAlign=\"stretch stretch\">\r\n                <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n                    <mat-card-title>SMS (Twilio) To Job Converter</mat-card-title>\r\n                </div>\r\n                <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n                    <mat-card-subtitle> Authorising ServiceM8... </mat-card-subtitle>\r\n                </div>\r\n                <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n                    <p style=\"text-align: center\"> SMS (Twilio) To Job Converter is now authorising your ServiceM8 account</p>\r\n                </div>\r\n\r\n                <div fxLayout=\"column\" style=\"margin-top: 130px\" fxLayoutAlign=\"end center\">\r\n                    <mat-card-content>\r\n                        <mat-spinner></mat-spinner>\r\n                        <!-- <button mat-raised-button color='primary' (click)=\"onClick()\">Register ServiceM8 Account</button> -->\r\n                    </mat-card-content>\r\n                </div>\r\n            </div>\r\n        </mat-card>\r\n    </div>\r\n</div>"

/***/ }),

/***/ 241:
/***/ (function(module, exports) {

module.exports = "<div style=\"width:(100%); height: (100%)\">\r\n    <div fxLayoutAlign=\"center center\">\r\n        <mat-card style=\"width:400px; min-height: 400px\">\r\n            <div fxLayout=\"column\" fxLayoutAlign=\"stretch stretch\">\r\n                <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n                    <mat-card-title>SMS (Twilio) To Job Converter</mat-card-title>\r\n                </div>\r\n                <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n                    <mat-card-subtitle> Authorise ServiceM8 </mat-card-subtitle>\r\n                </div>\r\n                <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n                    <p style=\"text-align: center\"> Welcome to the SMS (Twilio) To Job Converter Add-On</p>\r\n                </div>\r\n                <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n                    <p style=\"text-align: center\"> To authorise the app's access to your account, click the button below </p>\r\n                </div>\r\n                <div fxLayout=\"column\" style=\"margin-top: 130px\" fxLayoutAlign=\"end center\">\r\n                    <mat-card-content>\r\n\r\n                        <button mat-raised-button color='primary' (click)=\"onClick()\">Register ServiceM8 Account</button>\r\n\r\n                    </mat-card-content>\r\n                </div>\r\n            </div>\r\n        </mat-card>\r\n    </div>\r\n</div>"

/***/ }),

/***/ 242:
/***/ (function(module, exports) {

module.exports = "<div style=\"width:(100%); height: (100%)\">\r\n    <div fxLayoutAlign=\"center center\">\r\n        <mat-card style=\"width:400px; min-height: 400px\">\r\n            <div fxLayout=\"column\" fxLayoutAlign=\"stretch stretch\">\r\n                <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n                    <mat-card-title>SMS (Twilio) To Job Converter</mat-card-title>\r\n                </div>\r\n                <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n                    <mat-card-subtitle> Success </mat-card-subtitle>\r\n                </div>\r\n                <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n                    <p style=\"text-align: center\"> SMS (Twilio) To Job Converter should now be active</p>\r\n                </div>\r\n\r\n                <div fxLayout=\"column\" style=\"margin-top: 130px\" fxLayoutAlign=\"end center\">\r\n                    <mat-card-content>\r\n                        <button mat-raised-button color='primary' (click)=\"onClick()\">Back to ServiceM8</button>\r\n                    </mat-card-content>\r\n                </div>\r\n            </div>\r\n        </mat-card>\r\n    </div>\r\n</div>"

/***/ }),

/***/ 243:
/***/ (function(module, exports) {

module.exports = "<div style=\"width:(100%); height: (100%)\">\r\n    <div fxLayoutAlign=\"center center\">\r\n        <mat-card style=\"width:500px; min-height: 500px\">\r\n            <div fxLayout=\"column\" fxLayoutAlign=\"stretch stretch\">\r\n                <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n                    <mat-card-title>SMS (Twilio) To Job Converter</mat-card-title>\r\n                </div>\r\n                <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n                    <mat-card-subtitle> Messaging Setup... </mat-card-subtitle>\r\n                </div>\r\n                <div fxLayout=\"column\" fxLayoutAlign=\"center center\">\r\n                    <mat-card-content>\r\n                        <div fxLayout=\"column\" style=\"padding-left: 4px\" fxLayoutGap=\"16px\" fxLayoutAlign=\"start start\">\r\n                            <div fxLayout=\"column\" fxLayoutGap=\"8px\">\r\n                                <p>\r\n                                    What Twilio number will this apply to?\r\n                                </p>\r\n                                <mat-form-field>\r\n                                    <mat-select placeholder=\"Twilio Numbers\" [(ngModel)]=\"selectedTwilioNumber\" required>\r\n                                        <mat-option *ngFor=\"let number of twilioNumbers\" [value]=\"number\">\r\n                                            {{number}}\r\n                                        </mat-option>\r\n                                    </mat-select>\r\n                                </mat-form-field>\r\n\r\n                            </div>\r\n\r\n                            <div fxLayout=\"column\" fxLayoutGap=\"8px\">\r\n                                <p>\r\n                                    What category will the Twilio-made job be?\r\n                                </p>\r\n                                <mat-form-field>\r\n                                    <mat-select placeholder=\"ServiceM8 Categories\" [(ngModel)]=\"selectedJobCategory\" required>\r\n                                        <mat-option *ngFor=\"let category of servicem8Categories\" [value]=\"category\">\r\n                                            {{category}}\r\n                                        </mat-option>\r\n                                    </mat-select>\r\n                                </mat-form-field>\r\n\r\n                            </div>\r\n\r\n                            <div fxLayout=\"column\" fxLayoutGap=\"8px\">\r\n                                <mat-checkbox color=\"primary\" (change)=\"jobStatusReplySelect()\">Allow customer to text '!STATUS [JOB_ID]' to get job status?</mat-checkbox>\r\n                                <mat-checkbox color=\"primary\" (change)=\"jobScheduleReplySelect()\">Allow customer to text '!WHEN [JOB_ID]' to get scheduled date job?</mat-checkbox>\r\n                            </div>\r\n\r\n                            <div fxLayout=\"column\" fxLayoutGap=\"8px\">\r\n                                <mat-checkbox color=\"primary\" (change)=\"jobCreationReplySelect()\">Use custom auto reply message on ServiceM8 job creation?</mat-checkbox>\r\n\r\n                                <div *ngIf=\"jobCreationReply\" fxLayout=\"column\" fxLayoutAlign=\"center center\">\r\n                                    <p style=\"text-align: center\">\r\n                                        <b>\r\n                                            <u> Job Creation Reply SMS Text </u>\r\n                                        </b>\r\n                                    </p>\r\n                                    <p style=\"text-align: center\">\r\n                                        <b>Note: </b> you can use \"[JOB_ID]\" to represent the job ID in your message.\r\n                                    </p>\r\n                                    <mat-form-field style=\"width: 380px\" class=\"example-full-width\">\r\n                                        <textarea rows=\"8\" color=\"primary\" matInput [(ngModel)]=\"creationReplyText\"></textarea>\r\n                                    </mat-form-field>\r\n\r\n                                    <mat-error style=\"margin-top: 8px\" *ngIf=\"jobCreationReplyError\">You must provide an SMS message when a job is created.</mat-error>\r\n                                </div>\r\n\r\n                                <div *ngIf=\"!jobCreationReply\" fxLayout=\"column\" fxLayoutAlign=\"center center\">\r\n                                    <p style=\"text-align: center\">\r\n                                        <b>\r\n                                            <u> Default Message </u>\r\n                                        </b>\r\n                                    </p>\r\n                                    <p style=\"text-align: center; border: 1px black solid\">\r\n                                        <i>{{default_message}} </i>\r\n                                    </p>\r\n                                </div>\r\n                            </div>\r\n\r\n                        </div>\r\n                    </mat-card-content>\r\n                </div>\r\n\r\n\r\n            </div>\r\n\r\n            <mat-card-actions>\r\n                <div fxLayoutAlign=\"center center\">\r\n                    <button mat-raised-button color='primary' (click)=\"saveOptions()\">Save</button>\r\n                </div>\r\n            </mat-card-actions>\r\n        </mat-card>\r\n    </div>\r\n</div>"

/***/ }),

/***/ 244:
/***/ (function(module, exports) {

module.exports = "<link rel=\"stylesheet\" href=\"assets/forms.css\">\r\n<div style=\"width:(100%); height: (100%)\">\r\n  <div fxLayoutAlign=\"center center\">\r\n\r\n\r\n\r\n\r\n    <mat-card style=\"width:400px; min-height: 400px\">\r\n\r\n      <div fxLayout=\"column\" fxLayoutAlign=\"stretch stretch\">\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n          <mat-card-title>SMS (Twilio) To Job Converter</mat-card-title>\r\n        </div>\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n          <mat-card-subtitle> Twilio Registration </mat-card-subtitle>\r\n        </div>\r\n\r\n\r\n        <div fxLayout=\"column\" fxLayoutAlign=\"center center\">\r\n          <mat-card-content>\r\n            <form [formGroup]=\"twilioForm\" #formDir=\"ngForm\">\r\n\r\n              <div [hidden]=\"formDir.submitted\">\r\n\r\n                <label>SID NUMBER\r\n                  <span *ngIf=\"!SidToken.invalid\" class=\"glyphicon glyphicon-ok \" style=\"color:rgb(73, 233, 81)\"></span>\r\n                </label>\r\n                <mat-form-field class=\"form-group\" style=\"width: 380px\" class=\"example-full-width\">\r\n\r\n                  <input color=\"primary\" id=\"SidToken\" matInput [(ngModel)]=\"SidToken.value\" placeholder=\"\" formControlName=\"SidToken\" required>\r\n                </mat-form-field>\r\n                <div *ngIf=\"SidToken.invalid && (SidToken.dirty || SidToken.touched)\" class=\"alert alert-danger\">\r\n\r\n                  <div *ngIf=\"SidToken.errors.required\">\r\n                    Sid Number is required.\r\n                  </div>\r\n                  <div *ngIf=\"SidToken.errors.minlength\">\r\n                    Sid Number Shorter then 34 characters\r\n                  </div>\r\n                  <div *ngIf=\"SidToken.errors.pattern\">\r\n                    This is not a valid Twilio Sid Number\r\n                  </div>\r\n\r\n                </div>\r\n\r\n                <label>Auth Token\r\n                  <span *ngIf=\"!AuthToken.invalid\" class=\"glyphicon glyphicon-ok \" style=\"color:rgb(73, 233, 81)\"></span>\r\n                </label>\r\n                <mat-form-field class=\"form-group\" style=\"width: 380px\" class=\"example-full-width\">\r\n\r\n                  <input color=\"primary\" id=\"AuthToken\" matInput [(ngModel)]=\"AuthToken.value\" formControlName=\"AuthToken\" required>\r\n                </mat-form-field>\r\n                <div *ngIf=\"AuthToken.invalid && (AuthToken.dirty || AuthToken.touched)\" class=\"alert alert-danger\">\r\n\r\n                  <div *ngIf=\"AuthToken.errors.required\">\r\n                    Sid Number is required.\r\n                  </div>\r\n                  <div *ngIf=\"AuthToken.errors.minlength\">\r\n                    Sid Number Shorter then 32 characters\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </form>\r\n          </mat-card-content>\r\n        </div>\r\n        <mat-card-actions>\r\n          <div fxLayoutAlign=\"center center\">\r\n            <button mat-raised-button color='primary' type=\"submit\" [disabled]=\"twilioForm.invalid\" (click)=\"register()\">Register</button>\r\n\r\n          </div>\r\n        </mat-card-actions>\r\n      </div>\r\n    </mat-card>\r\n  </div>\r\n</div>"

/***/ }),

/***/ 288:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(139);


/***/ }),

/***/ 40:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngrx_store__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__(63);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return vendorUUID; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return vendorName; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return categoriesServiceM8; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return numbersTwilio; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var vendorUUID = function (state, _a) {
    if (state === void 0) { state = []; }
    var type = _a.type, payload = _a.payload;
    switch (type) {
        case 'SAVE_VENDOR':
            return payload;
        default:
            return state;
    }
};
var vendorName = function (state, _a) {
    if (state === void 0) { state = {}; }
    var type = _a.type, payload = _a.payload;
    switch (type) {
        case 'SAVE_VENDOR_NAME':
            return payload;
        default:
            return state;
    }
};
var categoriesServiceM8 = function (state, _a) {
    if (state === void 0) { state = []; }
    var type = _a.type, payload = _a.payload;
    switch (type) {
        case 'SAVE_CATEGORIES_SERVICEM8':
            return payload;
        default:
            return state;
    }
};
var numbersTwilio = function (state, _a) {
    if (state === void 0) { state = []; }
    var type = _a.type, payload = _a.payload;
    switch (type) {
        case 'SAVE_TWILIO_NUMBER':
            return payload;
        default:
            return state;
    }
};
var AuthService = /** @class */ (function () {
    function AuthService(store, http, router) {
        var _this = this;
        this.store = store;
        this.http = http;
        this.router = router;
        this.vendorUUID = this.store.select('vendorUUID');
        this.vendorUUID.subscribe(function (s) { return _this.vendorUUIDSnapshot = s; });
        this.categoriesServiceM8 = this.store.select('categoriesServiceM8');
        this.categoriesServiceM8.subscribe(function (s) { return _this.categoriesServiceM8Snapshot = s; });
        this.numbersTwilio = this.store.select('numbersTwilio');
        this.numbersTwilio.subscribe(function (s) { return _this.numbersTwilioSnapshot = s; });
        this.vendorName = this.store.select('vendorName');
        this.vendorName.subscribe(function (s) { return _this.vendorNameSnapshot = s; });
    }
    AuthService.prototype.dispatchVendorUUID = function (vendor) {
        console.log("Saving: " + vendor);
        this.store.dispatch({ type: 'SAVE_VENDOR', payload: vendor });
        console.log("Testing " + this.vendorUUIDSnapshot);
    };
    AuthService.prototype.registerTwilio = function (identifier, twilioSID, twilioAuthToken) {
        var _this = this;
        var body = {
            vendor_id: identifier,
            auth_token: twilioAuthToken,
            sid: twilioSID
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_4__angular_http__["Headers"]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_4__angular_http__["RequestOptions"]({ headers: headers });
        this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].apiUrl + '/twilio/register', body, options)
            .map(function (res) { return res.json(); })
            .subscribe(function (data) {
            //need to save categories and available numbers to store here
            console.log("DATA: " + JSON.stringify(data));
            console.log("ATTEMPTING TO STORE CATEGORIES: " + data.categories);
            _this.store.dispatch({ type: 'SAVE_CATEGORIES_SERVICEM8', payload: data.categories });
            console.log(_this.categoriesServiceM8Snapshot);
            console.log("ATTEMPTING TO STORE TWILIO NUMBERS: " + data.numbers);
            _this.store.dispatch({ type: 'SAVE_TWILIO_NUMBER', payload: data.numbers });
            console.log(_this.numbersTwilioSnapshot);
            _this.store.dispatch({ type: 'SAVE_VENDOR_NAME', payload: data.vendor_name });
            _this.router.navigate(['twilio-messaging-options']);
        });
    };
    AuthService.prototype.saveTwilioOptions = function (identifier, selectedTwilioNumber, jobCreation, selectedJobCategory, jobCreationReply, creationReplyText, statusReply, scheduleReply) {
        var _this = this;
        var body = {
            vendor_id: identifier,
            selectedTwilioNumber: selectedTwilioNumber,
            jobCreation: jobCreation,
            selectedJobCategory: selectedJobCategory,
            jobCreationReply: jobCreationReply,
            creationReplyText: creationReplyText,
            statusReply: statusReply,
            scheduleReply: scheduleReply
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_4__angular_http__["Headers"]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_4__angular_http__["RequestOptions"]({ headers: headers });
        this.http.post(__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].apiUrl + '/twilio/options', body, options)
            .map(function (res) { return res; })
            .subscribe(function (data) {
            _this.router.navigate(['auth-success']);
        });
    };
    AuthService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__ngrx_store__["b" /* Store */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__ngrx_store__["b" /* Store */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__angular_http__["Http"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_http__["Http"]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_5__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__angular_router__["a" /* Router */]) === "function" && _c || Object])
    ], AuthService);
    return AuthService;
    var _a, _b, _c;
}());

//# sourceMappingURL=auth.store.js.map

/***/ }),

/***/ 41:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
var environment = {
    production: true,
    apiUrl: 'https://servicem8twiliosyncapi.azurewebsites.net/api',
    hostUrl: 'https://servicem8twiliosync.azurewebsites.net',
    serverUrl: 'https://servicem8twiliosyncapi.azurewebsites.net/'
    // apiUrl: 'http://advancednotificationapi.azurewebsites.net/api',
    // hostUrl: 'http://advancednotification.azurewebsites.net',
    // serverUrl: 'http://advancednotificationapi.azurewebsites.net/'
};
//# sourceMappingURL=environment.js.map

/***/ })

},[288]);
//# sourceMappingURL=main.bundle.js.map